FROM --platform=linux/amd64 centos:centos8
CMD ['/bin/bash']
MAINTAINER Atos Mtools Team
ARG PROM_VERSION
ARG OS=linux
ARG ARCH=amd64

RUN curl -L https://github.com/prometheus/prometheus/releases/download/v$PROM_VERSION/prometheus-$PROM_VERSION.$OS-$ARCH.tar.gz  | tar zxof - \
	--xform="s~prometheus-$PROM_VERSION.$OS-$ARCH/prometheus.yml~/etc/prometheus/prometheus.yml~" \
	--xform="s~prometheus-$PROM_VERSION.$OS-$ARCH/promtool~/bin/promtool~" \ 
	--xform="s~prometheus-$PROM_VERSION.$OS-$ARCH/prometheus~/bin/prometheus~" \ 
	--xform="s~prometheus-$PROM_VERSION.$OS-$ARCH/tsdb~/bin/tsdb~" \ 
	--xform="s~prometheus-$PROM_VERSION.$OS-$ARCH~/usr/share/prometheus~" -C / 

USER nobody
WORKDIR /var/lib/prometheus
RUN chown nobody:nobody /var/lib/prometheus

EXPOSE 9090
VOLUME ["/etc/prometheus", "/usr/share/prometheus/consoles", "/usr/share/prometheus/console_libraries", "/var/lib/prometheus" ]

ENTRYPOINT ["/bin/prometheus"]
CMD ["--config.file=/etc/prometheus/prometheus.yml", "--storage.tsdb.path=/var/lib/prometheus", "--web.console.libraries=/usr/share/prometheus/console_libraries", "--web.console.templates=/usr/share/prometheus/consoles"]
